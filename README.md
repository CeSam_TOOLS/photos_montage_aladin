photos_montage_aladin
======================

# Authors

CeSAM (Centre de donnéeS Astrophysique de Marseille) at LAM (Laboratoire d'Astrophysique de Marseille)

# About photos_montage_aladin:

photos_montage_aladin is a SHELL script use to launch [aladin](https://aladin.u-strasbg.fr/aladin-f.gml) with Docker.

This project allow to build a docker image with last `aladin` jar version in order to retrieve automatically images of areas of space (see template_inputs/areas.csv file format) for differents surveys (see template_inputs/surveys.dat file format) and to assemble theses images into one or more mosaics with `imagemagick` tool.

The photos_montage_aladin repository is composed of:

- `conf-montage` folder containing Dockerfile to build the docker image of photos_montage_aladin.
- `template_inputs` folder containing template input files (__areas.csv__ and __surveys.dat__)
- script `photos_montage_aladin.sh` for linux users
- script `photos_montage_aladin_osx.sh` for MAC-OS users
- `.env` file containing environment variables useful for scripts
- `README.md` the readme file


here we have example of template input files format that we must respect imperatively:

-`areas.csv`:
```
id,name,la,ld,ra,dec
878357,HIGALBM348.1809-0.4702,348.180903,0.470223,258.04739,-38.521825
878358,HIGALBM349.1809-0.4802,349.180903,0.480223,258.04739,-38.521825
etc ...
```


-`surveys.dat`:
```
CDS/P/DSS2/red
CDS/P/2MASS/J
CDS/P/2MASS/H
CDS/P/2MASS/K
CDS/P/2MASS/color
CDS/P/allWISE/W1
CDS/P/allWISE/W2
CDS/P/allWISE/W3
CDS/P/allWISE/W4
CDS/P/allWISE/color
CDS/P/SPITZER/IRAC1
CDS/P/SPITZER/IRAC2
CDS/P/SPITZER/IRAC3
CDS/P/SPITZER/IRAC4
CDS/P/SPITZER/color
ESAVO/P/HERSCHEL/PACS70norm
ESAVO/P/HERSCHEL/PACS160norm
ESAVO/P/HERSCHEL/SPIRE250norm
ESAVO/P/HERSCHEL/SPIRE350norm
ESAVO/P/HERSCHEL/SPIRE500norm
CDS/P/ATLASGAL
```



To launch photos_montage_aladin there are two scripts:
- `photos_montage_aladin.sh` for linux users
- `photos_montage_aladin_osx.sh` for MAC-OS users

The photos_montage_aladin script `photos_montage_aladin.sh` (or `photos_montage_aladin_osx.sh` for MAC-OS users) provides output folder `datas_photos_montage_aladin` on personnal home user folder (__$HOME__ for linux users, __/Users/$USER__ for MAC-OS users ). this output folder contains a folder for each area in areas.csv input file.

For an area folder whe have:
- `images` folder containing images
- `montages` folder containing the mosaic(s)
- `area_[AREA_NAME].ajs` self-generated file containing macro command for aladin
- `[AREA_NAME].log` log of aladin
- `infos_[AREA_NAME].txt` informations about building images and montages

__*note*__: The user could choose an other output folder than the default output folder (see `--output_folder` option).

# Requirements

**WARNING**: As photos_montage_aladin scripts use `"Bash associative array"`, you must have __bash 4.0+__

__*note*__: to see which version of the software you are using:
```
which bash
bash --version
```

1. For Linux users:

  * You must have docker on your computer.(see [linux docker installation](https://docs.docker.com/install/))


2. For MAC-OS users:

  * You must have docker on your mac.(see [Mac-OS docker installation](https://docs.docker.com/docker-for-mac/install/))

  __*note*__: don't forget to add folders we want to mount  on Docker preferences (preferences -> File Sharing -> +)


# Installation

1. Clone the photos_montage_aladin repository:
```
git clone git@gitlab.lam.fr:CeSam_TOOLS/photos_montage_aladin.git
```

2. Launch the `photos_montage_aladin.sh` (or `photos_montage_aladin_osx.sh` for MAC-OS users) to create photos_montage_aladin docker image, build and launch photos_montage_aladin docker container :
```
./photos_montage_aladin.sh
```


# How to use photos_montage_aladin

To see all options available for a good use of `photos_montage_aladin.sh` (or `photos_montage_aladin_osx.sh` for MAC-OS users) make:
```
./photos_montage_aladin.sh -h OR --help
```

### Here are the options available in detail:

1. Get photos_montage_aladin script information, options available:
```
./photos_montage_aladin.sh -h OR --help
```
2. Get photos_montage_aladin image version available on Aladin server and on localhost and Options by default define on .env file:
```
./photos_montage_aladin.sh -i OR --infos
```
3. Build photos_montage_aladin image with selected VERSION:
```
./photos_montage_aladin.sh -b VERSION OR --build VERSION
```
4. Select a specific photos_montage_aladin image VERSION:
```
./photos_montage_aladin.sh --select VERSION
```
5. force the script to re-run all areas in input csv
```
./photos_montage_aladin.sh --force-all
```
6. force the script to re-run a specific area of the input csv
```
./photos_montage_aladin.sh --force AREA
```
7. Update the current photos_montage_aladin image:
```
./photos_montage_aladin.sh -u OR --update
```
8. Remove photos_montage_aladin image with selected VERSION:
```
./photos_montage_aladin.sh -rm VERSION OR --remove VERSION
```
9. Give script photos_montage_aladin.sh current version number:
```
./photos_montage_aladin.sh -v OR --version
```

### Here are the others options available `only` with :
```
./photos_montage_aladin.sh
or
./photos_montage_aladin.sh --select VERSION
```
__*note*__: in other case the others options have no results or effects

1. run photos_montage_aladin with this survey file (FILE) instead of
the default file define on `.env` file. the survey file must be on format '.dat' and respect the surveys.dat file format:
```
./photos_montage_aladin.sh --survey FILE
```
2. run photos_montage_aladin with this area file (FILE) instead of
the default file define on `.env` file. the area file must be on format '.csv' and respect the areas.csv file format:
```
./photos_montage_aladin.sh --area FILE
```
3. run photos_montage_aladin with this output folder (FOLDER) instead of the default folder define on .env file. The user must have the rights to write on the output folder (FOLDER):
```
./photos_montage_aladin.sh --output_folder FOLDER
```
4. run photos_montage_aladin with angular dimension value instead of
the default angular value define on `.env` file. the angular value must be on second of arc:
```
./photos_montage_aladin.sh --angular NUMBER
```
5. run photos_montage_aladin with this number of pixel by columns for the Image Magick montages instead of the default value define on `.env` file:
```
./photos_montage_aladin.sh --resolution_col NUMBER
```
6. run photos_montage_aladin with this number of pixel by lines for the Image Magick montages instead of the default value define on `.env` file:
```
./photos_montage_aladin.sh --resolution_line NUMBER
```
7. run photos_montage_aladin with this number of tiles by columns for the Image Magick montages montages instead of the default value define on `.env` file:
```
./photos_montage_aladin.sh --nb_tile_col NUMBER
```
8. run photos_montage_aladin with this number of tiles by lines for the Image Magick montages montages instead of the default value define on `.env` file:
```
./photos_montage_aladin.sh --nb_tile_line NUMBER
```
9. run photos_montage_aladin with a volume mounted on docker container repository /mnt instead of no volume mounted define by default on `.env` file:
```
./photos_montage_aladin.sh --mount PATH
```
