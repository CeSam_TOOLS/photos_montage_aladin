#!/bin/bash

. ./.env

export LC_CTYPE=C
export LANG=C

#---------------------------#
#-------- functions --------#
#---------------------------#
function_message()
{
  # function_message: display message with input argument
  # input error_message to display

  type_message=$1
  message=$2
  option=$3
  color=$4
  status=$5

  if [ "$type_message" == "TYPE_VAL_ERROR" ]; then

    echo ""
    echo "------------------------------------"
    echo ""
    echo -e "${RED}========================================================="
    echo -e "WARNING => ${message}"
    echo -e "=========================================================${COLOR_OFF}"

  elif [ "$type_message" == "TYPE_MOUNT_ERROR" ]; then

    echo ""
    echo "------------------------------------"
    echo ""
    echo -e "${RED}==================================================================="
    echo -e "${message}"
    echo -e "on volume mounted : ${option}"
    echo -e "===================================================================${COLOR_OFF}"

  elif [ "$type_message" == "TYPE_MOUNT_INFOS" ]; then

    echo ""
    echo "------------------------------------"
    echo ""
    echo -e "${color}========================================================================="
    echo -e "                                 ${status}"
    echo -e ""
    echo -e "Volume mounted : ${option}"
    echo -e "${message}"
    echo ""
    echo -e "=========================================================================${COLOR_OFF}"


  elif [ "$type_message" == "TYPE_FILE_ERROR" ]; then

    echo ""
    echo "------------------------------------"
    echo ""
    echo -e "${RED}========================================================="
    echo -e "          ERROR with option '${option}' "
    echo -e ""
    echo -e "${message}"
    echo -e "=========================================================${COLOR_OFF}"
    echo ""
    echo "------------------------------------"

  elif [ "$type_message" == "TYPE_NOT_NUMBER" ]; then

    echo ""
    echo "------------------------------------"
    echo ""
    echo -e "${RED}========================================================="
    echo -e "          ERROR with option '${option}' "
    echo -e ""
    echo -e "${message}"
    echo -e "=========================================================${COLOR_OFF}"
    echo ""
    echo "------------------------------------"

  elif [ "$type_message" == "TYPE_HELP" ]; then

    echo ""
    echo "------------------------------------"
    echo ""
    echo -e "${GREEN}========"
    echo -e "= HELP ="
    echo -e "========${COLOR_OFF}"

  elif [ "$type_message" == "TYPE_UNKNOWN" ]; then
    echo ""
    echo -e "${RED}==============================="
    echo -e " error: unrecognized arguments "
    echo -e "===============================${COLOR_OFF}"

  elif [ "$type_message" == "TYPE_EXIST_VERSION" ]; then

    echo ''
    echo -e "${RED}===================================="
    echo -e "             WARNING"
    echo -e "====================================${COLOR_OFF}"
    echo ''
    echo '------------------------------------'
    echo ''
    echo "You must select an existing version"
    echo "            available on   "
    echo "   https://aladin.u-strasbg.fr   "
    echo ''
    echo 'To see the available version(s):'
    echo ''
    echo "Please make ./photos_montage_aladin.sh -i OR --infos"
    echo ''
    echo '------------------------------------'
    echo ''


  elif [ "$type_message" == "TYPE_FORCE_WARNING" ]; then

    echo ''
    echo -e "${RED}===================================="
    echo -e "             WARNING"
    echo -e "====================================${COLOR_OFF}"
    echo ''
    echo '------------------------------------'
    echo ''
    echo "With option --force you must select an existing area"
    echo ''
    echo 'Please see the available area(s) on:'
    echo ''
    echo "${message}"
    echo ''
    echo '------------------------------------'
    echo ''

  elif [ "$type_message" == "TYPE_DELETE_EXIST_VERSION" ]; then

    echo ''
    echo -e "${RED}===================================="
    echo -e "             WARNING"
    echo -e "====================================${COLOR_OFF}"
    echo ''
    echo '------------------------------------'
    echo ''
    echo "You must delete an existing version"
    echo "    available on the localhost     "
    echo ''
    echo 'To see the available version(s):'
    echo ''
    echo "Please make ./photos_montage_aladin.sh -i OR --infos"
    echo ''
    echo '------------------------------------'
    echo ''

  elif [ "$type_message" == "TYPE_UPDATE" ]; then

    echo ''
    echo '========================================'
    echo '     YOU ARE NOT USING THE LAST         '
    echo '            ALADIN VERSION              '
    echo '========================================'
    echo ''
    echo '=> A new ALADIN version is available:' ${message}
    echo ''
    echo 'COMMAND TO UPDATE ALADIN VERSION:'
    echo ''
    echo '=> ./photos_montage_aladin.sh -u OR --update'
    echo '------------------------------------'
    echo ''
    echo ''

  else

    echo "BLA BLA"

  fi

  if [[ "$type_message" == "TYPE_VAL_ERROR" || "$type_message" == "TYPE_HELP" || "$type_message" == "TYPE_UNKNOWN" ]]; then
    echo ""
    echo "------------------------------------"
    echo ""
    echo -e "use : ${BOLD_BEGIN}./photos_montage_aladin.sh${BOLD_END} [${ITALIC_BEGIN}OPTIONS${ITALIC_END}] [${ITALIC_BEGIN}OTHERS${ITALIC_END}]"
    echo -e ""
    echo -e ""
    echo -e "[${ITALIC_BEGIN}OPTIONS${ITALIC_END}]:"
    echo -e ""
    echo -e "${BOLD_BEGIN}-i${BOLD_END}, ${BOLD_BEGIN}--infos${BOLD_END}                        give informations about versions available on aladin server, "
    echo -e "                                   about photos_montage_aladin image present on localhost and about "
    echo -e "                                   Options by default define on .env file"
    echo -e ""
    echo -e "${BOLD_BEGIN}-b${BOLD_END} ${ITALIC_BEGIN}VERSION${ITALIC_END}, ${BOLD_BEGIN}--build${BOLD_END} ${ITALIC_BEGIN}VERSION${ITALIC_END}        build photos_montage_aladin image with selected ${ITALIC_BEGIN}VERSION${ITALIC_END}"
    echo -e ""
    echo -e "${BOLD_BEGIN}--select${BOLD_END} ${ITALIC_BEGIN}VERSION${ITALIC_END}                   run photos_montage_aladin with selected ${ITALIC_BEGIN}VERSION${ITALIC_END}"
    echo -e ""
    echo -e "${BOLD_BEGIN}--force-all ${BOLD_END}                       re-run all areas in input csv"
    echo -e ""
    echo -e "${BOLD_BEGIN}--force${BOLD_END} ${ITALIC_BEGIN}AREA${ITALIC_END}                       re-run for the ${ITALIC_BEGIN}AREA${ITALIC_END}, the ${ITALIC_BEGIN}AREA${ITALIC_END} must be in input csv"
    echo -e ""
    echo -e "${BOLD_BEGIN}-u${BOLD_END}, ${BOLD_BEGIN}--update ${BOLD_END}                      update photos_montage_aladin image version"
    echo -e ""
    echo -e "${BOLD_BEGIN}-rm${BOLD_END} ${ITALIC_BEGIN}VERSION${ITALIC_END}, ${BOLD_BEGIN}--remove${BOLD_END} ${ITALIC_BEGIN}VERSION${ITALIC_END}      remove photos_montage_aladin image with selected ${ITALIC_BEGIN}VERSION${ITALIC_END}"
    echo -e ""
    echo -e "${BOLD_BEGIN}-h, ${BOLD_BEGIN}--help${BOLD_END}                         show this help message and exit"
    echo -e ""
    echo -e "${BOLD_BEGIN}-v, ${BOLD_BEGIN}--version${BOLD_END}                      give photos_montage_aladin.sh current version number"
    echo -e ""
    echo -e ""
    echo -e "[${ITALIC_BEGIN}OTHERS${ITALIC_END}]: (available only with ${BOLD_BEGIN}no option${BOLD_END} OR with ${BOLD_BEGIN}--select option${BOLD_END})"
    echo -e ""
    echo -e "${BOLD_BEGIN}--survey${BOLD_END} ${ITALIC_BEGIN}FILE${ITALIC_END}                      run photos_montage_aladin with this surveys ${ITALIC_BEGIN}FILE${ITALIC_END} instead of "
    echo -e "                                   the default file define on .env file. The survey ${ITALIC_BEGIN}FILE${ITALIC_END}"
    echo -e "                                   must be on format '.dat'"
    echo -e ""
    echo -e "${BOLD_BEGIN}--area${BOLD_END} ${ITALIC_BEGIN}FILE${ITALIC_END}                        run photos_montage_aladin with this areas ${ITALIC_BEGIN}FILE${ITALIC_END} instead of "
    echo -e "                                   the default file define on .env file. the area ${ITALIC_BEGIN}FILE${ITALIC_END}"
    echo -e "                                   must be on format '.csv'"
    echo -e ""
    echo -e "${BOLD_BEGIN}--output_folder${BOLD_END} ${ITALIC_BEGIN}FOLDER${ITALIC_END}             run photos_montage_aladin with this output folder ${ITALIC_BEGIN}FOLDER${ITALIC_END} instead of "
    echo -e "                                   the default folder define on .env file. the user must have"
    echo -e "                                   the rights to write on the output folder ${ITALIC_BEGIN}FOLDER${ITALIC_END}"
    echo -e ""
    echo -e "${BOLD_BEGIN}--angular${BOLD_END} ${ITALIC_BEGIN}NUMBER${ITALIC_END}                   run photos_montage_aladin with this angular dimension value (${ITALIC_BEGIN}NUMBER${ITALIC_END}) "
    echo -e "                                   instead of the default angular value define on .env file."
    echo -e "                                   the angular value (${ITALIC_BEGIN}NUMBER${ITALIC_END}) must be on second of arc"
    echo -e ""
    echo -e "${BOLD_BEGIN}--resolution_col${BOLD_END} ${ITALIC_BEGIN}NUMBER${ITALIC_END}            run photos_montage_aladin with this ${ITALIC_BEGIN}NUMBER${ITALIC_END} of pixel by columns "
    echo -e "                                   for the Image Magick montages"
    echo -e ""
    echo -e "${BOLD_BEGIN}--resolution_line${BOLD_END} ${ITALIC_BEGIN}NUMBER${ITALIC_END}           run photos_montage_aladin with this ${ITALIC_BEGIN}NUMBER${ITALIC_END} of pixel by lines "
    echo -e "                                   for the Image Magick montages"
    echo -e ""
    echo -e "${BOLD_BEGIN}--nb_tile_col${BOLD_END} ${ITALIC_BEGIN}NUMBER${ITALIC_END}               run photos_montage_aladin with this ${ITALIC_BEGIN}NUMBER${ITALIC_END} of tiles by columns "
    echo -e "                                   for the Image Magick montages"
    echo -e ""
    echo -e "${BOLD_BEGIN}--nb_tile_line${BOLD_END} ${ITALIC_BEGIN}NUMBER${ITALIC_END}              run photos_montage_aladin with this ${ITALIC_BEGIN}NUMBER${ITALIC_END} of tiles by lines "
    echo -e "                                   for the Image Magick montages"
    echo -e ""
    echo -e "${BOLD_BEGIN}--mount${BOLD_END} ${ITALIC_BEGIN}PATH${ITALIC_END}                       run photos_montage_aladin with a volume mounted (${ITALIC_BEGIN}PATH${ITALIC_END}) "
    echo -e "                                   on docker container repository /mnt "
    echo -e ""
  fi

}


function_build_tab_hach()
{
  # function_build_tab_hach: retrieve and organize $@
  # on differents arrays
  # return:
  # - tab_keys: array of options present on $@
  # - tab_values: array of argument need for each option present on $@
  # - tab_weight: array of weight need for each option present on $@
  # - val_error: TRUE or FALSE depending on error
  # - message: in case of error, error message

  # ---------------------- #
  # ARRAY[key]=value
  #declaration of the ARRAY
  #containing the options
  #allowed by the program
  #and if there is an argument With
  #the option
  # => key : option
  # => value : None or argument

  declare -A ARRAY

  ARRAY["-i"]="None";
  ARRAY["--infos"]="None";
  ARRAY["-b"]="VERSION";
  ARRAY["--build"]="VERSION";
  ARRAY["--select"]="VERSION";
  ARRAY["--force-all"]="None";
  ARRAY["--force"]="AREA";
  ARRAY["-u"]="None";
  ARRAY["--update"]="None";
  ARRAY["-rm"]="VERSION";
  ARRAY["--remove"]="VERSION";
  ARRAY["-h"]="None";
  ARRAY["--help"]="None";
  ARRAY["-v"]="None";
  ARRAY["--version"]="None";
  ARRAY["--survey"]="PATH FILE SURVEYS";
  ARRAY["--area"]="PATH FILE AREAS";
  ARRAY["--output_folder"]="PATH OUTPUT FOLDER";
  ARRAY["--angular"]="NUMBER";
  ARRAY["--resolution_line"]="NUMBER";
  ARRAY["--resolution_col"]="NUMBER";
  ARRAY["--nb_tile_line"]="NUMBER";
  ARRAY["--nb_tile_col"]="NUMBER";
  ARRAY["--mount"]="PATH VOLUME";

  # ---------------------- #
  # ARRAY_WEIGHT[key]=value
  #declaration of the ARRAY_WEIGHT
  #containing the options
  #allowed by the program
  #and the weight of each option
  # => key : option
  # => value : weight

  declare -A ARGS_WEIGHT

  ARGS_WEIGHT["--select"]=0;
  ARGS_WEIGHT["--no_select"]=0;
  ARGS_WEIGHT["--force-all"]=1;
  ARGS_WEIGHT["--force"]=1;
  ARGS_WEIGHT["--update"]=1;
  ARGS_WEIGHT["-u"]=1;
  ARGS_WEIGHT["-i"]=1;
  ARGS_WEIGHT["--infos"]=1;
  ARGS_WEIGHT["-b"]=1;
  ARGS_WEIGHT["--build"]=1;
  ARGS_WEIGHT["-rm"]=1;
  ARGS_WEIGHT["--remove"]=1;
  ARGS_WEIGHT["-h"]=1;
  ARGS_WEIGHT["--help"]=1;
  ARGS_WEIGHT["-v"]=1;
  ARGS_WEIGHT["--version"]=1;
  ARGS_WEIGHT["--survey"]=2;
  ARGS_WEIGHT["--area"]=2;
  ARGS_WEIGHT["--output_folder"]=2;
  ARGS_WEIGHT["--angular"]=2;
  ARGS_WEIGHT["--resolution_line"]=2;
  ARGS_WEIGHT["--resolution_col"]=2;
  ARGS_WEIGHT["--nb_tile_line"]=2;
  ARGS_WEIGHT["--nb_tile_col"]=2;
  ARGS_WEIGHT["--mount"]=2;

  # -------- #

  indice=0;

  for params in "$@"
  do
    tab_params[$indice]=$params
    ((indice++));
  done

  # -------- #
  val_error="FALSE"
  elements_ok=0

  for elem in ${!ARRAY[*]} ; do

    indice=0;

    for param in "$@" #loop on input args
    do
      if [ "${elem}" == $param ]; then
        #if option of ARRAY is find among input argument

        elements_ok=$(($elements_ok + 1))
        tab_keys[$indice]=$elem
        tab_values[$indice]="None"
        tab_weight[$indice]=${ARGS_WEIGHT[${elem}]}

        if [ "${ARRAY[${elem}]}" != "None" ]; then
          #case argument needed for the option

          FOUND=$(echo ${!ARRAY[*]} | grep -- "${tab_params[$indice +1]}")

          if [[ ${tab_params[$indice +1]} != "" && $FOUND == "" && ${tab_params[$indice +1]} != -* ]]; then
            #case argument needed for the option is present on input argument
            tab_values[$indice]=${tab_params[$indice +1]}
            elements_ok=$(($elements_ok + 1))
          else
            #case argument needed for the option is missing on input argument
            message="Missing argument for the option "${elem}
            val_error="TRUE"
          fi

        fi

      fi

      ((indice++));

    done

  done

  if [ "$val_error" == "FALSE" ]; then

    if [ "$elements_ok" -ne "${#@}" ]; then

      val_error="TRUE"
      message="Option not found"
    else
      val_error="FALSE"
    fi
  fi

  echo "${tab_keys[@]},${tab_values[@]},${tab_weight[@]},$val_error,$message"

}

function_sort_tab()
{
  # function_sort_tab: sort input arrays depending on weight
  # inputs:
  # - tab_keys: array of options present on $@
  # - tab_values: array of argument need for each option present on $@
  # - tab_weight: array of weight need for each option present on $@
  # return:
  # - sort_arg_keys: array 'tab_keys' sorted by weight
  # - sort_arg_values: array 'tab_values' sorted by weight
  # - sort_arg_weight: array 'tab_weight' sorted by weight

  #--------------------------#
  # retrieving input arrays
  tab_arg_keys=$1
  tab_arg_values=$2
  tab_arg_weight=$3

  arg_keys=( ${tab_arg_keys// / } )
  arg_values=( ${tab_arg_values// / } )
  arg_weight=( ${tab_arg_weight// / } )

  #--------------------------#
  indice=0
  indice_weight=0
  l=0

  #--------------------------#
  # sorts input arrays

  while [ $indice -ne ${#arg_weight[@]} ];
   do
    if [ ${arg_weight[$l]} -eq $indice_weight ]; then
      sort_arg_keys[$indice]=${arg_keys[$l]}
      sort_arg_values[$indice]=${arg_values[$l]}
      sort_arg_weight[$indice]=${arg_weight[$l]}
      indice=$(($indice + 1))
      l=$(($l + 1))
    else
      l=$(($l + 1))
    fi

    if [ $l -eq ${#arg_weight[@]} ]; then
      l=0
      indice_weight=$(($indice_weight + 1))
    fi

   done

  #--------------------------#
  #return arrays sorted

  echo "${sort_arg_keys[@]},${sort_arg_values[@]},${sort_arg_weight[@]}"

}

function_return_indice_arg()
{
  # function_return_indice_arg: get the indice in input array of the element we want
  # input:
  # -tab: array of options present on $@
  # -find_arg: option we want find
  # return:
  # -indice of the option we want find

  #---------------#
  #input arguments
  tab=$1
  find_arg=$2
  #---------------#

  tempo_tab=( ${tab// / } )
  val="None"

  #------------------------------------#
  #loop to find indice of option we want
  for k in ${!tempo_tab[*]}
  do
    if [ "${tempo_tab[$k]}" == "$find_arg" ]; then
      val=$k
    fi
  done
  #------------------------------------#

  echo $val

}

function_check_and_replace()
{
# function_check_and_replace: check if input args is in tab and replace val
#
# inputs:
# -tab_keys: keys array of options present on $@
# -tab_keys: values array of options present on $@
# -find_arg: option we want find
# -val_arg: option value by default

tab_keys=$1
tab_values=$2
find_arg=$3
val_arg=$4

tempo_tab_keys=( ${tab_keys// / } )
tempo_tab_values=( ${tab_values// / } )

if [[ "${tempo_tab_keys[@]}" =~ $find_arg ]]; then
  indice=`function_return_indice_arg "${tempo_tab_keys[*]}" $find_arg`
  echo "${sort_arg_values[$indice]}"

else
  echo ${val_arg}
fi

}

function function_check_select_option()
{


#------------------------------------#
#check options presents
#to manage '--select' and '--no_select'
#options

tab_arg_keys=$1
tab_arg_values=$2
tab_arg_weight=$3
#------------------------------------#

tempo_arg_weight=( ${tab_arg_weight// / } )
tempo_arg_keys=( ${tab_arg_keys// / } )
taille=${#tempo_arg_weight[@]}

case $taille in
  "0")
    #---------------------#
    #case 0 input argument
    #---------------------#
    tab_arg_keys[0]="--no_select"
    tab_arg_values[0]="$ALADIN_VERSION"
    tab_arg_weight[0]=0
    arg_keys=( ${tab_arg_keys[@]// / } )
    arg_values=( ${tab_arg_values[@]// / } )
    arg_weight=( ${tab_arg_weight[@]// / } )

    ;;
  "1")
    #---------------------#
    #case 1 input argument
    #---------------------#
    if [ "${tempo_arg_weight[0]}" -eq 0 ]; then
      # case option with weight=0 ('--select')

      arg_keys=( ${tab_arg_keys// / } )
      arg_values=( ${tab_arg_values// / } )
      arg_weight=( ${tab_arg_weight// / } )


    elif [ "${tempo_arg_weight[0]}" -eq 1 ]; then
      #case option with weight=1

      if [[ "${tempo_arg_keys[0]}" == '--force-all' || "${tempo_arg_keys[0]}" == '--force' ]]; then
        #case '--force_all', '--force'

        tab2_arg_keys[0]="--no_select"
        tab2_arg_values[0]="$ALADIN_VERSION"
        tab2_arg_weight[0]=0
        tab2_arg_keys[1]=${tab_arg_keys}
        tab2_arg_values[1]=${tab_arg_values}
        tab2_arg_weight[1]=${tab_arg_weight}
        arg_keys=( ${tab2_arg_keys[@]// / } )
        arg_values=( ${tab2_arg_values[@]// / } )
        arg_weight=( ${tab2_arg_weight[@]// / } )

      else
        #case '-i','--infos','-u','--update','-h','-v','--version'

        arg_keys=( ${tab_arg_keys// / } )
        arg_values=( ${tab_arg_values// / } )
        arg_weight=( ${tab_arg_weight// / } )

      fi


    elif [ "${tempo_arg_weight[0]}" -eq 2 ]; then
        #case '--survey', '--area' ,'--angular'
        #'--resolution_line', '--resolution_col',
        #'--nb_tile_line','--nb_tile_col','--mount'

        tab2_arg_keys[0]="--no_select"
        tab2_arg_values[0]="$ALADIN_VERSION"
        tab2_arg_weight[0]=0
        tab2_arg_keys[1]=${tab_arg_keys}
        tab2_arg_values[1]=${tab_arg_values}
        tab2_arg_weight[1]=${tab_arg_weight}
        arg_keys=( ${tab2_arg_keys[@]// / } )
        arg_values=( ${tab2_arg_values[@]// / } )
        arg_weight=( ${tab2_arg_weight[@]// / } )


    else

      echo '=> No case possible for the moment'

    fi

    ;;
  *)
    #-----------------------------#
    #case 2 or more input arguments
    #-----------------------------#
    for p in ${!tempo_arg_keys[*]}
    do

      if [ ${tempo_arg_weight[$p]} -eq 0 ]; then
        tempo_weight_0="TRUE"
      elif [ ${tempo_arg_weight[$p]} -eq 1 ]; then
        tempo_weight_1="TRUE"
        indice_tempo_weight_1=$p
      elif [ ${tempo_arg_weight[$p]} -eq 2 ]; then
        tempo_weight_2="TRUE"
      fi

    done

    if [[ "$tempo_weight_0" == 'TRUE' && "$tempo_weight_2" == 'FALSE' ]]; then
      #case '--select'
      arg_keys=( ${tab_arg_keys// / } )
      arg_values=( ${tab_arg_values// / } )
      arg_weight=( ${tab_arg_weight// / } )
    fi

    if [ "$tempo_weight_1" == 'TRUE' ]; then

      if [ "${tempo_arg_keys[$indice_tempo_weight_1]}" == '--force-all' ]; then
        #case '--force_all'

        tab2_arg_keys[0]="--no_select"
        tab2_arg_values[0]="$ALADIN_VERSION"
        tab2_arg_weight[0]=0
        tab2_arg_keys[1]=${tab_arg_keys}
        tab2_arg_values[1]=${tab_arg_values}
        tab2_arg_weight[1]=${tab_arg_weight}
        arg_keys=( ${tab2_arg_keys[@]// / } )
        arg_values=( ${tab2_arg_values[@]// / } )
        arg_weight=( ${tab2_arg_weight[@]// / } )

      else
        #case '-i','--infos','-u','--update','-h','-v','--version'

        arg_keys=( ${tab_arg_keys// / } )
        arg_values=( ${tab_arg_values// / } )
        arg_weight=( ${tab_arg_weight// / } )

      fi


    fi

    if [ "$tempo_weight_2" == 'TRUE' ]; then
      #case '--survey', '--area' ,'--angular'
      #'--resolution_line', '--resolution_col',
      #'--nb_tile_line','--nb_tile_col', '--mount'

        tab2_arg_keys[0]="--no_select"
        tab2_arg_values[0]="$ALADIN_VERSION"
        tab2_arg_weight[0]=0
        tab2_arg_keys[1]=${tab_arg_keys}
        tab2_arg_values[1]=${tab_arg_values}
        tab2_arg_weight[1]=${tab_arg_weight}
        arg_keys=( ${tab2_arg_keys[@]// / } )
        arg_values=( ${tab2_arg_values[@]// / } )
        arg_weight=( ${tab2_arg_weight[@]// / } )



    fi

    ;;
esac

  echo "${arg_keys[@]},${arg_values[@]},${arg_weight[@]}"


}

function_check_number()
{
  # function_check_number: return 'TRUE' if input arg is a
  # number, if not "FALSE"

  var=$1

  #if [[ $var = +([0-9]) ]] ; then
  if [[ $var =~ ^[+]?[0-9]+$ ]] ; then
    reponse="TRUE"

  else
    reponse="FALSE"
  fi

  echo "$reponse"

}

function_check_permission_volume()
{
  # function_check_permission_volume: check if user have read and write
  # permissions for the input volume.
  # input: volume
  # returns:
  #    return TRUE if permission OK else FALSE
  volume=$1

  volume_permissions=$(ls -ld $volume | tail -n 1 | awk -F" " '{print $1}')
  volume_type=${volume_permissions:0:1}
  volume_owner_permissions=${volume_permissions:1:3}
  volume_group_permissions=${volume_permissions:4:3}
  volume_others_permissions=${volume_permissions:7:3}
  volume_owner=$(ls -ld $volume | tail -n 1 | awk -F" " '{print $3}')
  volume_group_owner=$(ls -ld $volume | tail -n 1 | awk -F" " '{print $4}')

  user_name=$(whoami)
  user_groups=$(id | awk -F " " '{print $3}' | sed 's/^groupes//g' | sed 's/[0-9()=]//g')
  tab_user_groups=( ${user_groups//,/ } )

  if [[ $user_name == $volume_owner ]]; then
  # -----------------------------------------#
  #       case user is volume owner
  # -----------------------------------------#

    if [[ $volume_owner_permissions == "rwx" || $volume_owner_permissions == "rw-" ]]; then
    #case user could read and write on volume

      AUTHORIZATION="TRUE"

    else
    #case user could not read and write on volume
    #we check group authorization

      if [[ "${tab_user_groups[@]}" =~ "$volume_group_owner" ]]; then
      #case group volume is in user group list

        if [[ $volume_group_permissions == "rwx" || $volume_group_permissions == "rw-" ]]; then
        #case group volume allow user to read and write
          AUTHORIZATION="TRUE"

        else
        #case group volume not allow user to read and write
        #we check other volume permissions

          if [[ $volume_others_permissions == "rwx" || $volume_others_permissions == "rw-" ]]; then
          #case other permissions volume allow user to read and write
            AUTHORIZATION="TRUE"

          else
          #case other permissions volume not allow user to read and write
            AUTHORIZATION="FALSE"

          fi

        fi

      else
        #case group volume is not in user group list
        #we check other volume permissions

        if [[ $volume_others_permissions == "rwx" || $volume_others_permissions == "rw-" ]]; then
        #case other permissions volume allow user to read and write
          AUTHORIZATION="TRUE"

        else
        #case other permissions volume not allow user to read and write
          AUTHORIZATION="FALSE"

        fi

      fi

    fi

  else
  # -----------------------------------------#
  #     case user is not  volume owner
  # -----------------------------------------#

      if [[ "${tab_user_groups[@]}" =~ "$volume_group_owner" ]]; then
      #case group volume is in user group list

        if [[ $volume_group_permissions == "rwx" || $volume_group_permissions == "rw-" ]]; then
        #case group volume allow user to read and write
          AUTHORIZATION="TRUE"

        else
        #case group volume not allow user to read and write
        #we check other volume permissions

          if [[ $volume_others_permissions == "rwx" || $volume_others_permissions == "rw-" ]]; then
          #case other permissions volume allow user to read and write
            AUTHORIZATION="TRUE"

          else
          #case other permissions volume not allow user to read and write
            AUTHORIZATION="FALSE"

          fi

        fi

      else
        #case group volume is not in user group list
        #we check other volume permissions

        if [[ $volume_others_permissions == "rwx" || $volume_others_permissions == "rw-" ]]; then
        #case other permissions volume allow user to read and write
          AUTHORIZATION="TRUE"

        else
        #case other permissions volume not allow user to read and write
          AUTHORIZATION="FALSE"

        fi

      fi


  fi


      echo "$AUTHORIZATION"

}

function_jar_updated_search()
{
  # function_jar_updated_search: get the last jar version
  # returns:
  #    return number_version

  curl -s ${ALADIN_URL}"/nph-aladin.pl?frame=downloading" > .temp
  sed -i '' '/Official version/!d' .temp
  version=$(cut -d">" -f3 .temp)
  rm .temp

  number_version=$(echo ${version:1})
  echo "$number_version"

}

function_delete_image()
{
  # function_delete_image: delete the image
  # params:
  #    $1 - photos_montage_aladin version image to delete

  echo "- Deleting the photos_montage_aladin-$1 docker image ..."

  old_id=$(docker inspect --format="{{.Id}}" image_photos_montage_aladin-$1)
  docker rmi ${old_id} #remove old photos_montage_aladin image

  echo "=> Deleting the image_photos_montage_aladin-$1 docker image successfully"

}

function_build_image()
{
  # function_build_image: build the photos_montage_aladin image

      version=$1

      #----------------------------------------#
      curl -s ${ALADIN_URL}"/nph-aladin.pl?frame=downloading" > .tempo
      sed -i '' '/Official version/!d' .tempo
      awk -F">" '{print $3}' .tempo > .tempo5
      sed -i '' 's/v//' .tempo5
      curl -s ${ALADIN_URL}"/nph-aladin.pl?frame=downloading" > .tempo2
      sed -i '' '/<!--/d' .tempo2
      sed -i '' '/last/!d' .tempo2
      awk -F">" '{print $3}' .tempo2 > .tempo3
      awk -F"<" '{print $1}' .tempo3 > .tempo4
      sed -i '' 's/\.jar//' .tempo4
      sed -i '' 's/Aladin//' .tempo4
      cat .tempo4 >> .tempo5
      list_jar=$(sort -r .tempo5 | awk '{print $1}')
      rm .tempo*

      present=$(echo $list_jar| grep "${version}")

      if [ $? -eq 1 ]; then

        function_message "TYPE_EXIST_VERSION"
        exit 0


      else

        echo ''
        echo "- Searching for the current photos_montage_aladin-${version} docker image ..."

        id_image=$(docker inspect --format="{{.Id}}" image_photos_montage_aladin-${version})

        if [ ${id_image} ]; then

          echo "=> The image_photos_montage_aladin-${version} docker image already exist"
          echo ''

        else

          #----------------------------------------#
          curl -s ${ALADIN_URL}"/nph-aladin.pl?frame=downloading" > .temp
          sed -i '' '/Official version/!d' .temp
          tempo=$(cut -d">" -f3 .temp)
          rm .temp

          aladin_last_number_version=$(echo ${tempo:1})
          #----------------------------------------#

          if [ ${aladin_last_number_version} == ${version} ]; then
            wget -o .logfile -P conf-montage ${ALADIN_URL}/Aladin.jar --no-check-certificate
            mv conf-montage/Aladin.jar conf-montage/Aladin-${version}.jar
          else
            wget -o .logfile -P conf-montage ${ALADIN_URL}/Aladin${version}.jar --no-check-certificate
            mv conf-montage/Aladin${version}.jar conf-montage/Aladin-${version}.jar

          fi

          #-----------------#
          #build
          echo "- Creating the photos_montage_aladin-${version} docker image in progress ..."
          echo '------------------------------------------------------------------'

          docker build --build-arg username=$USER --build-arg uidval=$UID --build-arg home=/home/$USER --build-arg aladin_version=${version} -t image_photos_montage_aladin-${version} conf-montage

          echo "=> creating the photos_montage_aladin-${version} docker image successfully"
          echo '------------------------------------------------------------------'
          echo ''
          #-----------------#

          rm conf-montage/Aladin-${version}.jar
          rm .logfile

        fi
      fi

}



function_create_ajs_file()
{
  # function_create_ajs_file : create ajs surveys file

inputfile=$1
outputfile=$2
area=$3
ra_area=$4
dec_area=$5
outputfolder=$6


list_survey=$(awk '{print $1}' $inputfile)

echo "reset" > ${outputfile}

number=0;

for survey in ${list_survey}
do
    i=0;
    ((number++));

    echo "#" >> ${outputfile}
    echo "rm @1" >> ${outputfile}
    echo "get HiPS("$survey", FITS)" ${ra_area} ${dec_area} ${ANGULAR}"'" >> ${outputfile}
    echo "cm BB localcut" >> ${outputfile}


    for val in `echo $survey | tr "/" " "`;
    do
     ((i++));
     tab[$i]=${val}
    done

    if [ $number -lt '10' ]; then
      chaine="0${number}_${area}-${tab[$i-1]}_${tab[$i]}.${FORMAT_IMAGES}"
    else
      chaine="${number}_${area}-${tab[$i-1]}_${tab[$i]}.${FORMAT_IMAGES}"
    fi
    echo "save 200x200 ${outputfolder}/$chaine" >> ${outputfile}


done



}
#-----------------------------------------#
#----------- main program ----------------#
#-----------------------------------------#

#---------------------#
# management arguments
# call function_sort_tab

RESULTAT=`function_build_tab_hach $@`

tab_arg_keys=`echo $RESULTAT | cut -d "," -f 1`
tab_arg_values=`echo $RESULTAT | cut -d "," -f 2`
tab_arg_weight=`echo $RESULTAT | cut -d "," -f 3`
val_error=`echo $RESULTAT | cut -d "," -f 4`

#------------------------------------#
#check if val_error is TRUE

if [ $val_error == "TRUE" ]; then
  error_message=`echo $RESULTAT | cut -d "," -f 5`

  function_message "TYPE_VAL_ERROR" "${error_message}"
  exit 0
fi

#----------------------------------------------------#
# check to manage '--select' and '--no_select options
# call function_check_select_option

RESULT_CHECK_SELECT=`function_check_select_option "${tab_arg_keys[*]}" "${tab_arg_values[*]}" "${tab_arg_weight[*]}"`

array_arg_keys=`echo $RESULT_CHECK_SELECT | cut -d "," -f 1`
array_arg_values=`echo $RESULT_CHECK_SELECT | cut -d "," -f 2`
array_arg_weight=`echo $RESULT_CHECK_SELECT | cut -d "," -f 3`

arg_keys=( ${array_arg_keys// / } )
arg_values=( ${array_arg_values// / } )
arg_weight=( ${array_arg_weight// / } )


#--------------------------#
# sort the arrays arguments
# call function_sort_tab

RESULT_SORT=`function_sort_tab "${arg_keys[*]}" "${arg_values[*]}" "${arg_weight[*]}"`

tab_sort_arg_keys=`echo $RESULT_SORT | cut -d "," -f 1`
tab_sort_arg_values=`echo $RESULT_SORT | cut -d "," -f 2`
tab_sort_arg_weight=`echo $RESULT_SORT | cut -d "," -f 3`

sort_arg_keys=( ${tab_sort_arg_keys// / } )
sort_arg_values=( ${tab_sort_arg_values// / } )
sort_arg_weight=( ${tab_sort_arg_weight// / } )

#------------------------------#
#check volume_mount input args

VOLUME_MOUNT=`function_check_and_replace "${sort_arg_keys[*]}" "${sort_arg_values[*]}" "--mount" $VOLUME_MOUNT`

if [ $VOLUME_MOUNT != "NONE" ];then
  if [[ ! -d $VOLUME_MOUNT ]]; then
    function_message "TYPE_FILE_ERROR" "$VOLUME_MOUNT not found" "--mount"
      exit 0
  fi
fi

#------------------------------#
#check survey input args

INPUT_FILE_SURVEYS=`function_check_and_replace "${sort_arg_keys[*]}" "${sort_arg_values[*]}" "--survey" $INPUT_FILE_SURVEYS`

if [ ! -f $INPUT_FILE_SURVEYS ]; then
  function_message "TYPE_FILE_ERROR" "$INPUT_FILE_SURVEYS not found" "--survey"
    exit 0
fi

#------------------------------#
#check area input args

INPUT_FILE_AREAS=`function_check_and_replace "${sort_arg_keys[*]}" "${sort_arg_values[*]}" "--area" $INPUT_FILE_AREAS`

if [ ! -f $INPUT_FILE_AREAS ]; then
  function_message "TYPE_FILE_ERROR" "$INPUT_FILE_AREAS not found" "--area"
    exit 0
fi
#------------------------------#
#check output_folder args

OUTPUT_FOLDER_ON_VOLUME_MOUNT="FALSE"
OLD_OUTPUT_FOLDER_MACOS=$OUTPUT_FOLDER_MACOS

OUTPUT_FOLDER_MACOS=`function_check_and_replace "${sort_arg_keys[*]}" "${sort_arg_values[*]}" "--output_folder" $OUTPUT_FOLDER_MACOS`

result_check_permission="TRUE"

#----------------------------------------#
#check if OUTPUT_FOLDER is on VOLUME_MOUNT

if [[ $OUTPUT_FOLDER_MACOS != $OLD_OUTPUT_FOLDER_MACOS && $VOLUME_MOUNT != "NONE" ]]; then

  CHAINE=$(echo $OUTPUT_FOLDER_MACOS | grep "^$VOLUME_MOUNT")

  if [ "$CHAINE" != "" ]; then
    OUTPUT_FOLDER_ON_VOLUME_MOUNT="TRUE"

    result_check_permission=`function_check_permission_volume $VOLUME_MOUNT`

  fi

fi
#----------------------------------------#


if [ ! -d $OUTPUT_FOLDER_MACOS ]; then
  function_message "TYPE_FILE_ERROR" "$OUTPUT_FOLDER_MACOS not found" "--output_folder"
    exit 0
fi

#-------------------------------------#
#check angular input arg

ANGULAR=`function_check_and_replace "${sort_arg_keys[*]}" "${sort_arg_values[*]}" "--angular" $ANGULAR`

ANGULAR_IS_NUMBER=`function_check_number "$ANGULAR"`

if [ $ANGULAR_IS_NUMBER != 'TRUE' ]; then

  function_message "TYPE_NOT_NUMBER" "ANGULAR must be an integer" "--angular"
  exit 0

fi

#-------------------------------------#
#check resolution line input args

RESOLUTION_BY_LINE=`function_check_and_replace "${sort_arg_keys[*]}" "${sort_arg_values[*]}" "--resolution_line" $RESOLUTION_BY_LINE`

RESOLUTION_BY_LINE_IS_NUMBER=`function_check_number "$RESOLUTION_BY_LINE"`

if [ $RESOLUTION_BY_LINE_IS_NUMBER != 'TRUE' ]; then

  function_message "TYPE_NOT_NUMBER" "RESOLUTION_BY_LINE must be an integer" "--resolution_line"
  exit 0


fi

#-------------------------------------#
#check resolution col input args

RESOLUTION_BY_COL=`function_check_and_replace "${sort_arg_keys[*]}" "${sort_arg_values[*]}" "--resolution_col" $RESOLUTION_BY_COL`

RESOLUTION_BY_COL_IS_NUMBER=`function_check_number "$RESOLUTION_BY_COL"`

if [ $RESOLUTION_BY_COL_IS_NUMBER != 'TRUE' ]; then

  function_message "TYPE_NOT_NUMBER" "RESOLUTION_BY_COL must be an integer" "--resolution_col"
  exit 0


fi

#-------------------------------------#
#check number of tile by line input args

NB_TILE_BY_LINE=`function_check_and_replace "${sort_arg_keys[*]}" "${sort_arg_values[*]}" "--nb_tile_line" $NB_TILE_BY_LINE`

NB_TILE_BY_LINE_IS_NUMBER=`function_check_number "$NB_TILE_BY_LINE"`

if [ $NB_TILE_BY_LINE_IS_NUMBER != 'TRUE' ]; then

  function_message "TYPE_NOT_NUMBER" "NB_TILE_BY_LINE must be an integer" "--nb_tile_line"
  exit 0


fi

#-------------------------------------#
#check number of tile by col input args

NB_TILE_BY_COL=`function_check_and_replace "${sort_arg_keys[*]}" "${sort_arg_values[*]}" "--nb_tile_col" $NB_TILE_BY_COL`

NB_TILE_BY_COL_IS_NUMBER=`function_check_number "$NB_TILE_BY_COL"`

if [ $NB_TILE_BY_COL_IS_NUMBER != 'TRUE' ]; then

  function_message "TYPE_NOT_NUMBER" "NB_TILE_BY_COL must be an integer" "--nb_tile_col"
  exit 0


fi

#---------------------#
# --no_select management

if [ "${sort_arg_keys[0]}" == "--no_select" ]; then
  sort_arg_keys[0]=""
  sort_arg_values[0]=""
  sort_arg_weight[0]=0
fi

#---------------------#

#=============================================================#
#=============================================================#
#=============================================================#



case ${sort_arg_keys[0]} in
    ""|"--select")
      ###################################
      # use: ./photos_montage_aladin.sh #
      ###################################
      version_init=$(cat .env | grep ALADIN_VERSION)

      if [ ${version_init} == "ALADIN_VERSION=?" ]; then

        init='TRUE'

        echo "INITIALISATION VERSION"

        #------------------------------------------#
        #call function function_jar_updated_search

        aladin_last_number_version=`function_jar_updated_search`
        #----------------------------------------#
        sed -i '' "s/ALADIN_VERSION=?/ALADIN_VERSION=${aladin_last_number_version}/" .env

      else

        init='FALSE'

      fi
      # ---------------------------------------- #

      if [ ${init} == TRUE ]; then

        version=${aladin_last_number_version}

      else
        if [ "${sort_arg_keys[0]}" == "--select" ]; then

          # --------------- #
          curl -s ${ALADIN_URL}"/nph-aladin.pl?frame=downloading" | sed '/Official version/!d' | cut -d">" -f3 | sed 's/v//' > .temp
          curl -s ${ALADIN_URL}"/nph-aladin.pl?frame=downloading" | sed '/<!--/d'| sed '/last/!d' | cut -d">" -f3 | cut -d"<" -f1 | sed 's/\.jar//' | sed 's/Aladin//' >> .temp
          list_jar=$(sort -r .temp | awk '{print $1}')
          rm .temp
          present=$(echo $list_jar| grep "${sort_arg_values[0]}")

          if [ $? -eq 1 ]; then

            function_message "TYPE_EXIST_VERSION"
            exit 0

          else

            version=${sort_arg_values[0]}

          fi
          # --------------- #

        else

          version=${ALADIN_VERSION}

        fi

      fi

      echo -e "${BOLD_BEGIN}"
      echo -e "------------------------------------"
      echo -e "        Photos Montage Aladin       "
      echo -e ""
      echo -e "- aladin version: ${version}${BOLD_END}"

      #----------------------------------------#
      #check result_check_permission

      if [ $result_check_permission != 'TRUE' ]; then

        function_message "TYPE_MOUNT_ERROR" "you do not have permission to write and / or read" $VOLUME_MOUNT
        exit 0

      else

        if [ $VOLUME_MOUNT == "NONE" ]; then
          volume_used="/Users/$USER"
        else
          volume_used=$VOLUME_MOUNT
        fi

        info_volume_total=$(df -h ${volume_used} | tail -n 1 | awk -F " " '{print $2}')
        info_volume_free=$(df -h ${volume_used} | tail -n 1 | awk -F " " '{print $4}')
        info_percentage=$(df -h ${volume_used} | tail -n 1 | awk -F " " '{print $5}')
        info_percentage_number=$(df -h ${volume_used} | tail -n 1 | awk -F " " '{print $5}'| sed 's/%//')

        if [ $(( $info_percentage_number )) -lt ${WARNING_LEVEL_DISK_SPACE_PERCENTAGE} ]; then
          function_message "TYPE_MOUNT_INFOS" "Disk Space: $info_volume_total, Disk Space Free: $info_volume_free, Space disk pourcentage used: $info_percentage" $volume_used $GREEN "INFOS"

        elif [[ $(( $info_percentage_number )) -gt ${WARNING_LEVEL_DISK_SPACE_PERCENTAGE} && $(( $info_percentage_number )) -lt ${CRITICAL_LEVEL_DISK_SPACE_PERCENTAGE} ]]; then
          function_message "TYPE_MOUNT_INFOS" "Disk Space: $info_volume_total, Disk Space Free: $info_volume_free, Space disk pourcentage used: $info_percentage" $volume_used $YELLOW "WARNING"
          sleep 5
        else
          function_message "TYPE_MOUNT_INFOS" "Disk Space: $info_volume_total, Disk Space Free: $info_volume_free, Space disk pourcentage used: $info_percentage" $volume_used $RED "WARNING CRITICAL"
          exit 0
        fi

      fi


      #----------------------------------------#
      #call function function_jar_updated_search

      aladin_last_number_version=`function_jar_updated_search`

      #----------------------------------------#

      if [ ${aladin_last_number_version} != ${version} ]; then

        function_message "TYPE_UPDATE" "${aladin_last_number_version}"
        sleep 5

      else
        #case no update

        echo -e "${BOLD_BEGIN}"
        echo -e "------------------------------------"
        echo -e "${BOLD_END}"

      fi

      #----------------------------------------#
      #call function function_build_image

      if [ ${version} == ${ALADIN_VERSION} ]; then
        function_build_image ${ALADIN_VERSION}
      else
        function_build_image $version
      fi

      #------------------------#
      #creation of main_folder

      main_folder=${OUTPUT_FOLDER_MACOS}"/datas_photos_montage_aladin"

      if [ -d ${main_folder} ]; then
        echo "=> ${main_folder} exist"
        echo " "
      else
        mkdir ${main_folder}
        echo "=> creation of " ${main_folder}
        echo " "

      fi

      #------------------------#

      indice=0

      while read enreg
      do
        if [ $enreg != "id,name,la,ld,ra,dec" ]; then
          list_zone[$indice]=`echo $enreg | awk -F"," '{print $2}'`
          list_zone[$indice]=${list_zone[$indice]}"_ANG"${ANGULAR}
          list_ra[$indice]=`echo $enreg | awk -F"," '{print $5}'`
          list_dec[$indice]=`echo $enreg | awk -F"," '{print $6}'`
          ((indice++));
        fi
      done < ${INPUT_FILE_AREAS}

      #------------------------#
      nb_lines_survey=`wc -l $INPUT_FILE_SURVEYS | awk '{print $1}'`

      tempo_tile=`expr $NB_TILE_BY_LINE \* $NB_TILE_BY_COL `
      tempo2_tile=`expr ${nb_lines_survey}/${tempo_tile}`
      nb_tile=$((${tempo2_tile%.*}+1))



      #------------------------#
      #loop on areas

      for k in ${!list_zone[*]}
      do

      echo "============================="
      echo -e "${BOLD_BEGIN}${list_zone[$k]}${BOLD_END}"
      echo "============================="

      #------------------------#
        if [ "${sort_arg_keys[1]}" == "--force-all" ]; then
          force='TRUE'
          force_all='TRUE'
          force_warning='FALSE'
        elif [[ "${sort_arg_keys[1]}" == "--force" && "${sort_arg_values[1]}" == ${list_zone[$k]} ]]; then
          force='TRUE'
          force_all='FALSE'
          force_warning='FALSE'
        elif [[ "${sort_arg_keys[1]}" == "--force" && "${sort_arg_values[1]}" != ${list_zone[$k]} ]]; then
          force='FALSE'
          force_all='FALSE'
          force_warning='TRUE'
        else
          force='FALSE'
        fi

        #------------------------#
        #check OUTPUT_FOLDER_ON_VOLUME_MOUNT

        if [ $OUTPUT_FOLDER_ON_VOLUME_MOUNT == "TRUE" ]; then
          container_mount="/mnt/"
          new_main_folder=${container_mount}${OUTPUT_FOLDER_MACOS}"/datas_photos_montage_aladin"
        else
          container_mount=""
          new_main_folder="/home/"$USER"/datas_photos_montage_aladin"
        fi


        #------------------------#
        area_folder[$k]=${main_folder}"/"${list_zone[$k]}
        tempo_folder[$k]=${area_folder[$k]}
        images_folder[$k]=${area_folder[$k]}"/images"
        montages_folder[$k]=${area_folder[$k]}"/montages"
        area_ajs[$k]=${tempo_folder[$k]}"/area_"${list_zone[$k]}".ajs"
        log_aladin_file[$k]=${area_folder[$k]}"/"${list_zone[$k]}".log"
        infos_file[$k]=${area_folder[$k]}"/infos_"${list_zone[$k]}".txt"

        #------------------------#
        if [ -d ${area_folder[$k]} ]; then
          if [ $force == 'TRUE' ]; then
            rm -rf ${area_folder[$k]} #case --force

            if [ $force_all == 'TRUE' ]; then
              echo ""
              echo "* USE OF --force-all"
              echo ""
            else
              echo ""
              echo "* USE OF --force ${list_zone[$k]}"
              echo ""
            fi

            echo "=> remove of " ${area_folder[$k]}
            mkdir ${area_folder[$k]}
            echo "=> creation of " ${area_folder[$k]}
          else
            echo "=> ${area_folder[$k]} exist"
          fi
        else
          mkdir ${area_folder[$k]}
          echo "=> creation of " ${area_folder[$k]}
        fi
        #-----------------------------#
        #call function_create_ajs_file

        if [ -f ${area_ajs[$k]} ]; then
          echo "=> ${area_ajs[$k]} exist"
        else
          echo -e "=> ${RED}WARNING${COLOR_OFF} : ${area_ajs[$k]} not found"
          echo "=> construction of ${area_ajs[$k]} in progress ..."
          function_create_ajs_file ${INPUT_FILE_SURVEYS} ${area_ajs[$k]} ${list_zone[$k]} ${list_ra[k]} ${list_dec[k]} ${container_mount}${images_folder[$k]}
          echo "=> construction of ${area_ajs[$k]} OK"
          rm -rf ${images_folder[$k]}
          rm -rf ${montages_folder[$k]}

          if [ -f ${infos_file[$k]} ]; then
            rm ${infos_file[$k]}
          fi

          if [ -f ${log_aladin_file[$k]} ]; then
            rm ${infos_file[$k]}
          fi
        fi

        #---------------------------#
        #tests on images and montages
        #folders

        #--------#
        if [ -d ${images_folder[$k]} ]; then
          echo "=> ${images_folder[$k]} exist"
        else
          mkdir ${images_folder[$k]}
          echo "=> creation of " ${images_folder[$k]}
        fi
        #--------#
        if [ -d ${montages_folder[$k]} ]; then
          echo "=> ${montages_folder[$k]} exist"
        else
          mkdir ${montages_folder[$k]}
          echo "=> creation of " ${montages_folder[$k]}
        fi
        #--------#

        test_images_folder=$([ "$(ls -A ${images_folder[$k]})" ] && echo "Non vide" || echo "Vide")
        test_montages_folder=$([ "$(ls -A ${montages_folder[$k]})" ] && echo "Non vide" || echo "Vide")

        if [[ ${test_images_folder} == "Non vide" && ${test_montages_folder} == "Non vide" ]]; then
          nb_images[$k]=$(find ${images_folder[$k]} -type f | wc -l)
          find ${images_folder[$k]} -type f | wc -l > .temp1
          nb_images[$k]=$(sed "s/\ //g" .temp1)
          find ${montages_folder[$k]} -type f | wc -l > .temp2
          nb_montages[$k]=$(sed "s/\ //g" .temp2)

          rm .temp1
          rm .temp2

          if [[ ${nb_images[$k]} == $nb_lines_survey && ${nb_montages[$k]} == $nb_tile ]]; then
            state="FINISH"
            echo "=> All image(s) and montage(s) present(s) for " ${list_zone[$k]}
          else
            rm -rf ${images_folder[$k]}
            rm -rf ${montages_folder[$k]}

            mkdir ${images_folder[$k]}
            mkdir ${montages_folder[$k]}

            rm ${area_ajs[$k]}
            rm ${infos_file[$k]}
            rm ${log_aladin_file[$k]}

            echo "=> construction of ${area_ajs[$k]} in progress ..."
            function_create_ajs_file ${INPUT_FILE_SURVEYS} ${area_ajs[$k]} ${list_zone[$k]} ${list_ra[k]} ${list_dec[k]} ${container_mount}${images_folder[$k]}
            echo "=> construction of ${area_ajs[$k]} OK"
            state="RUN"

            if [ $force == 'FALSE' ]; then
              echo -e "=> ${RED}WARNING${COLOR_OFF} : Missing one or more images or montages"
            fi
          fi

        else
          rm -rf ${images_folder[$k]}
          rm -rf ${montages_folder[$k]}

          mkdir ${images_folder[$k]}
          mkdir ${montages_folder[$k]}

          state="RUN"
        fi

        #------------------------#

        if [ $state == "RUN" ]; then

          #----------------------------------------#
          #construction of docker script and montage
          echo "=> Construction in progress ..."

          new_area_folder[$k]=${new_main_folder}"/"${list_zone[$k]}
          new_tempo_folder[$k]=${new_area_folder[$k]}
          new_images_folder[$k]=${new_area_folder[$k]}"/images"
          new_montages_folder[$k]=${new_area_folder[$k]}"/montages"
          new_area_ajs[$k]=${new_tempo_folder[$k]}"/area_"${list_zone[$k]}".ajs"

          #------------------------#
          script_images[$k]="macro "${new_area_ajs[$k]}
          script_montages[$k]="montage -geometry ${RESOLUTION_BY_LINE}x${RESOLUTION_BY_COL} -tile ${NB_TILE_BY_LINE}x${NB_TILE_BY_COL} ${new_images_folder[$k]}/*${FORMAT_IMAGES} ${new_montages_folder[$k]}/${list_zone[$k]}.${FORMAT_MONTAGES}"



          #------------------------#

          xhost +
          if [[ "${sort_arg_keys[@]}" =~ "--mount" && $VOLUME_MOUNT != "NONE" ]]; then
            docker run --name photos_montage_aladin --rm -v /tmp/.X11-unix:/tmp/.X11-unix -v /Users/$USER:/home/$USER -v ${VOLUME_MOUNT}:"/mnt/${VOLUME_MOUNT}" --network host -e DISPLAY=host.docker.internal:0 -e USER=$USER -e HOME=/home/$USER image_photos_montage_aladin-${version} /bin/bash -c "java -jar /var/aladin/Aladin-${version}.jar -nogui -trace -script=${script_images[$k]};${script_montages[$k]} " > ${log_aladin_file[$k]} 2>&1
          else
            docker run --name photos_montage_aladin --rm -v /tmp/.X11-unix:/tmp/.X11-unix -v /Users/$USER:/home/$USER --network host -e DISPLAY=host.docker.internal:0 -e USER=$USER -e HOME=/home/$USER image_photos_montage_aladin-${version} /bin/bash -c "java -jar /var/aladin/Aladin-${version}.jar -nogui -trace -script=${script_images[$k]};${script_montages[$k]} " > ${log_aladin_file[$k]} 2>&1
          fi
          xhost -

          echo -e "=> ${GREEN}INFOS${COLOR_OFF} : Construction OK"
          #----------------------------------------#
          echo ""
          echo "=> For more informations about the construction please see:"
          echo ${log_aladin_file[$k]}
          echo ""


          #----------------------------------------#
          #construction of infos_file[$k]
          echo "===============================================" > ${infos_file[$k]}
          echo "" >> ${infos_file[$k]}
          echo "Infos construction of ${list_zone[$k]}          " >> ${infos_file[$k]}
          echo "" >> ${infos_file[$k]}
          echo "           $(date +'%Y-%m-%d %H-%M-%S')        " >> ${infos_file[$k]}
          echo "" >> ${infos_file[$k]}
          echo "===============================================" >> ${infos_file[$k]}
          echo "" >> ${infos_file[$k]}
          echo "--------------------" >> ${infos_file[$k]}
          echo "--- Aladin infos ---" >> ${infos_file[$k]}
          echo "--------------------" >> ${infos_file[$k]}
          echo "" >> ${infos_file[$k]}
          echo "Aladin version: $ALADIN_VERSION" >> ${infos_file[$k]}
          echo "" >> ${infos_file[$k]}
          echo "AJS file: ${area_ajs[$k]}" >> ${infos_file[$k]}
          echo "" >> ${infos_file[$k]}
          echo "Angular dimension value in second of Arc: ${ANGULAR}" >> ${infos_file[$k]}
          echo "" >> ${infos_file[$k]}
          echo "--------------------------" >> ${infos_file[$k]}
          echo "--- Image magick infos ---" >> ${infos_file[$k]}
          echo "--------------------------" >> ${infos_file[$k]}
          echo "" >> ${infos_file[$k]}
          echo "Tile dimension: $NB_TILE_BY_LINE x $NB_TILE_BY_COL" >> ${infos_file[$k]}
          echo "" >> ${infos_file[$k]}
          echo "Resolution: ${RESOLUTION_BY_LINE}x${RESOLUTION_BY_COL}" >> ${infos_file[$k]}
          echo "" >> ${infos_file[$k]}
          echo "-----------------------------------" >> ${infos_file[$k]}
          echo "--- photos_montage_aladin infos ---" >> ${infos_file[$k]}
          echo "-----------------------------------" >> ${infos_file[$k]}
          echo "" >> ${infos_file[$k]}
          echo "OPTIONS: $@" >> ${infos_file[$k]}
          echo "" >> ${infos_file[$k]}

          if [ $VOLUME_MOUNT != "NONE" ];then
          echo "VOLUME MOUNTED: $VOLUME_MOUNT" >> ${infos_file[$k]}
          else
          echo "VOLUME MOUNTED: NO VOLUME" >> ${infos_file[$k]}
          fi
          echo "" >> ${infos_file[$k]}

          echo "Input surveys file: $INPUT_FILE_SURVEYS" >> ${infos_file[$k]}
          echo "" >> ${infos_file[$k]}
          echo "number of surveys: $nb_lines_survey" >> ${infos_file[$k]}
          echo "" >> ${infos_file[$k]}
          echo "Input areas file: $INPUT_FILE_AREAS" >> ${infos_file[$k]}
          echo "" >> ${infos_file[$k]}
          echo "Number of areas: ${#list_zone[@]}" >> ${infos_file[$k]}
          echo "" >> ${infos_file[$k]}
          echo "Images folder: ${images_folder[$k]}" >> ${infos_file[$k]}
          echo "" >> ${infos_file[$k]}
          echo "Number of images: $(find ${images_folder[$k]} -type f | wc -l)" >> ${infos_file[$k]}
          echo "" >> ${infos_file[$k]}
          echo "Montages folder: ${montages_folder[$k]}" >> ${infos_file[$k]}
          echo "" >> ${infos_file[$k]}
          echo "Number of montages: $(find ${montages_folder[$k]} -type f | wc -l)" >> ${infos_file[$k]}
          echo "" >> ${infos_file[$k]}
          echo "log aladin file: ${log_aladin_file[$k]}" >> ${infos_file[$k]}
          echo "" >> ${infos_file[$k]}
          echo "===============================================" >> ${infos_file[$k]}

          #----------------------------------------#

        else

          echo -e "=> ${GREEN}INFOS${COLOR_OFF} : Nothing to do for ${area_folder[$k]} all files are present"
          echo " "
        fi

      done
      #------------------------#
      #------------------------#
      if [ "${force_warning}" == 'TRUE' ]; then

        function_message "TYPE_FORCE_WARNING" "${INPUT_FILE_AREAS}"
        exit 0

      fi


      #------------------------#

      echo ""
      echo "======================================="
      echo ""
      echo "=> End of photos_montage_aladin program"
      echo ""



      ;;

    "-b"|"--build")
      #################################
      # use: ./photos_montage_aladin.sh -b OR --build #
      #################################

      version=${sort_arg_values[0]}

      #---------------------------------#

      curl -s ${ALADIN_URL}"/nph-aladin.pl?frame=downloading" | sed '/Official version/!d' | cut -d">" -f3 | sed 's/v//' > .temp
      curl -s ${ALADIN_URL}"/nph-aladin.pl?frame=downloading" | sed '/<!--/d'| sed '/last/!d' | cut -d">" -f3 | cut -d"<" -f1 | sed 's/\.jar//' | sed 's/Aladin//' >> .temp
      list_jar=$(sort -r .temp | awk '{print $1}')
      rm .temp
      present=$(echo $list_jar| grep "${version}")

      if [ $? -eq 1 ]; then

        function_message "TYPE_EXIST_VERSION"
        exit 0
      fi


      #---------------------------------#
      list_jar=$(docker images | awk '/^image_photos_montage_aladin-/{print $1}')

      present=$(echo $list_jar| grep "${version}")

      if [ $? -eq 1 ]; then

        #------------------------------------#
        #call function function_build_image

        function_build_image ${sort_arg_values[0]}

        #------------------------------------#


      else

        #------------------------------------#
        #call function function_delete_image

        function_delete_image ${sort_arg_values[0]}

        #------------------------------------#
        #call function function_build_image

        function_build_image ${sort_arg_values[0]}

        #------------------------------------#

      fi


      ;;
    "-u"|"--update")
      ##################################################
      # use: ./photos_montage_aladin.sh -u OR --update #
      ##################################################

      #------------------------------------------#
      #call function function_jar_updated_search

      new_aladin_version=`function_jar_updated_search`
      old_aladin_version=${ALADIN_VERSION}

      if [ ${new_aladin_version} != ${old_aladin_version} ]; then
        #case update available
        wget -o .logfile -P conf-montage ${ALADIN_URL}/Aladin.jar
        mv conf-montage/Aladin.jar conf-montage/Aladin-${new_aladin_version}.jar

        sed -i "s/${old_aladin_version}/${new_aladin_version}/" .env

        echo "=> An update is available"
        echo "- new update version:" ${new_aladin_version}

        #-----------------------------------#
        #call function function_delete_image

        function_delete_image ${old_aladin_version}

        #-----------------------------------#
        #build

        echo "- Creating the photos_montage_aladin-${new_aladin_version} docker image in progress ..."
        echo '------------------------------------------------------------------'

        docker build --build-arg username=$USER --build-arg uidval=$UID --build-arg home=/home/$USER --build-arg aladin_version=${new_aladin_version} -t image_photos_montage_aladin-${new_aladin_version} conf-montage

        echo "=> creating the photos_montage_aladin-${new_aladin_version} docker image successfully"
        echo '------------------------------------------------------------------'
        echo ''

        #-----------------#

          rm conf-montage/Aladin-${new_aladin_version}.jar
          rm .logfile

      else
        #case no update

        echo '-----------------------------------------------------------'
        echo ''
        echo "- photos_montage_aladin use already the last aladin version"
        echo '- aladin version: ' ${ALADIN_VERSION}
        echo ''
        echo '-----------------------------------------------------------'

      fi


      ;;
    "-i"|"--infos")
      #################################################
      # use: ./photos_montage_aladin.sh -i OR --infos #
      #################################################

      curl -s ${ALADIN_URL}"/nph-aladin.pl?frame=downloading" | sed '/Official version/!d' | cut -d">" -f3 | sed 's/v//' > .temp
      curl -s ${ALADIN_URL}"/nph-aladin.pl?frame=downloading" | sed '/<!--/d'| sed '/last/!d' | cut -d">" -f3 | cut -d"<" -f1 | sed 's/\.jar//' | sed 's/Aladin//' >> .temp
      list_jar=$(sort -n -r .temp | awk '{print $1}')
      rm .temp

      id_image=$(docker inspect --format="{{.Id}}" image_photos_montage_aladin-${ALADIN_VERSION})

      echo ""
      echo "-------------------------------------------------------------------"
      echo -e "        ${UNDERLINE_BEGIN}aladin stable versions available on :${UNDERLINE_END}"
      echo ""
      echo -e "${RED}- Aladin server : ${ALADIN_URL}${RED}"
      echo -e "${GREEN}- Localhost${COLOR_OFF}"

      echo "-------------------------------------------------------------------"
      echo ""

      for number_version in ${list_jar}
      do
        if [ -z $id_image ]; then
          echo -e "   ${RED}   ${number_version}${COLOR_OFF}"
        else
          if [ ${number_version} == ${ALADIN_VERSION} ]; then
            echo -e "   ${GREEN} *${COLOR_OFF} ${RED}${number_version}${COLOR_OFF}"
          else
            echo -e "   ${RED}   ${number_version}${COLOR_OFF}"
          fi
        fi
      done

      echo ""
      echo "-------------------------------------------------------------------"
      echo ""
      echo -e "        ${UNDERLINE_BEGIN}Options by default define on .env file:${UNDERLINE_END}"
      echo ""
      echo "- INPUT_FILE_SURVEYS: ${INPUT_FILE_SURVEYS}"
      echo "- INPUT_FILE_AREAS: ${INPUT_FILE_AREAS}"
      echo "- ANGULAR: ${ANGULAR}"
      echo "- NB_TILE_BY_LINE: ${NB_TILE_BY_LINE}"
      echo "- NB_TILE_BY_COL: ${NB_TILE_BY_COL}"
      echo "- RESOLUTION_BY_LINE: ${RESOLUTION_BY_LINE}"
      echo "- RESOLUTION_BY_COL: ${RESOLUTION_BY_COL}"
      echo "- VOLUME_MOUNT: ${VOLUME_MOUNT}"
      echo ""
      echo -e '\e[4munderline\e[24m'

      echo "-------------------------------------------------------------------"
      echo ""


      ;;

    "-v"|"--version")
      ###################################################
      # use: ./photos_montage_aladin.sh -v OR --version #
      ###################################################

      echo '- photos_montage_aladin.sh version: ' ${SCRIPT_VERSION}

      #---------------------------------#
      ;;

    "-rm"|"--remove=")
      ###################################################
      # use: ./photos_montage_aladin.sh -rm OR --remove #
      ###################################################

      version=${sort_arg_values[0]}

      list_image_version=$(docker images | awk '/^image_photos_montage_aladin-/{print $1}')

      present=$(echo $list_image_version| grep $version)

      if [ $? -eq 1 ]; then

        function_message "TYPE_DELETE_EXIST_VERSION"
        exit 0

      else

        function_delete_image $version

      fi
      #---------------------------------#
      ;;
    "-h"|"--help")
      ################################################
      # use: ./photos_montage_aladin.sh -h OR --help #
      ################################################

      function_message "TYPE_HELP"
      ;;

    *)
      #################################################
      # use: ./photos_montage_aladin.sh OTHER COMMAND #
      #################################################

      function_message "TYPE_UNKNOWN"
      ;;

esac


#-----------------------------------------#
#-----------------------------------------#
#-----------------------------------------#
